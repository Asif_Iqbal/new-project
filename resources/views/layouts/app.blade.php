<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Blog</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/other.css') }}" rel="stylesheet">
</head>
<body>
    @php
                    $post = \App\Post::where('user_id',Auth::id())
                     ->first();
    @endphp
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm navbar-color">
            <div class="container">
                <a class="asif" href="{{ url('/blog_post') }}">
                   BLOG 
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                        @isset($post->photos->photo)
                        <li> <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image" style="height:30px;border-radius:50%;margin-top:5px;"></li>
                        @endisset
                        @empty($post->photos->photo)
                        <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image" style="height:30px;border-radius:50%;margin-top:5px;">
                        @endempty

                        {{-- @if($post)
                        <li> <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image" style="height:30px;border-radius:50%;margin-top:5px;"></li>
                        @else
                        <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image" style="height:30px;border-radius:50%;margin-top:5px;">
                        @endif --}}
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle nav-name" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <span class="nav-name">
                                    {{ Auth::user()->name }} <span class="caret"> </span>
                                        </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right nav-color" aria-labelledby="navbarDropdown">

                                      @if (Auth::user()->isAdmin())
                                        <a href="{{ route('users.index') }}" class="dropdown-item">Dashboard</a>
                                        @endif
                                        <a href="{{ route('blog_post.home') }}" class="dropdown-item">Blog</a>
                                        <a href="{{ url('photo') }}" class="dropdown-item">Upload Image</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        {{-- <main class="py-4"> --}}
            @yield('content')
        {{-- </main> --}}
    </div>
</body>
</html>

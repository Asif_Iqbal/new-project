@extends('layouts.app')
@section('content')

{{-- @foreach ($posts as $post) --}}

@foreach ($posts as $post)
<div class="title">
        <h5>
                @if (Session::has('comment_message'))
                {{ session('comment_message') }}
                @endif
            </h5>
        <h1>{{ $post->title }}</h1>
        <p>By:{{ $post->user->name }}</p>

    </div>
<div class="container">

    <div class="row">
        <div class="col-sm-6">

    <div class="image">
            @isset($post->photos->photo)
            <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image"style="width:100%">
           @endisset

           @empty($post->photos->photo)
           <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image" style="height:30px;border-radius:50%;margin-top:5px;">
           @endempty
        </div>
        </div>
        <div class="col-sm-6 body_part">
           <div class="body1">
                <p>{{ $post->body }}
                </p>



            </div>
        </div>

        <div class="link">
        {{ $posts->links() }}
    </div>
    </div>
    @endforeach



    <form action="{{ route('comment.store')}}" method="post">
        @csrf
            <div class="comments">
                <input type="hidden" name="post_id" value="{{ $post->id }}" >
    <div class="form-group">
        <textarea type="input" name="body" class="form-control" rows="5"></textarea>
      </div>
      <div class="form-group">
            <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
</form>

@php
    $comments = \App\Comment::whereIsActive(1)->get();

@endphp
{{-- @php
    $photo = \App\Photo::whereIsActive(1)->get();

@endphp --}}
    {{-- @if (count($comments)>0) --}}

    {{-- @foreach ($comments as $comment) --}}

    @foreach ($comments as $comment)
<div class="media border p-3">


        <img src="{{ asset('images/'.$post->photos->photo) }}" alt="John Doe" class="mr-3 mt-3 rounded-circle" style="width:60px;">
        <div class="media-body">
          <h4>{{ $comment->author }}<small><i class="time">{{$comment->created_at->diffForHumans() }}</i></small></h4>
          <p>{{  $comment->body }}</p>






          {{-- comment reply --}}

          <form action="{{ route('comment_reply.store')}}" method="post">
            @csrf
                <div class="comments">
                    <input type="hidden" name="comment_id" value="{{ $comment->id }}" >
        <div class="form-group">
            <textarea type="input" name="body" class="form-control" rows="1"></textarea>
          </div>
          <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
    </form>

    @php
    $comment_reply = \App\CommentReply::all();

    @endphp


            @foreach($comment_reply as $commentans)


          <div class="media p-3">
            <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="Jane Doe" class="mr-3 mt-3 rounded-circle" style="width:45px;">
            <div class="media-body">
              <h4>{{ $commentans->author }} <small><i>{{$commentans->created_at->diffForHumans() }}</i></small></h4>
              <p>{{  $commentans->body}}</p>
            </div>
          </div>
        </div>
    </div>

    @endforeach
    @endforeach






@stop

@extends('admin')

@section('content1')
<style>
h1{
    text-align: center;
    color: crimson;
    font-weight: 900;

}

</style>

<div class="container">
        <h1>User Detailes</h1>



<table class="table">
<tr>
    <th>Name</th>
    <th>Email</th>
    <th>Role</th>
    <th>Active</th>
</tr>
@foreach ($users as $user)
<tr>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    @if ($user->role)
    <td>{{ $user->role->name}}</td>
    @else
    <th></th>
    @endif

    <td>{{ $user->is_active==1 ? 'Active':'Not active' }}</td>
</tr>
@endforeach


</table>
</div>


@stop

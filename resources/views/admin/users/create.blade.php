@extends('admin')

@section('content1')
<style>
.form-group{
    width: 70%;
    padding-left: 15%;
}

h1{
    text-align: center;
}
</style>
<div class="container">
    <h1>Create Users</h1>

    <form action="{{ url('admin/users') }}" method="POST">
        <div class="form-group">
          <label for="name">Name:</label>
          <input type="name" class="form-control" name="name">
        </div>
        <div class="form-group">
          <label for="email">Email:</label>
          <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
                <label for="role_id">Role:</label>

               
             
                <select class="form-control">
                    <option>Choose a option</option> 
                    @foreach ($roles as $item)
                    <option>{{ $item->name }}</option> 
                    @endforeach         
                   </select>
                
                  
                    
                  
                      
              </div>
              <div class="form-group">
                    <label for="is_active">Active:</label>
                    <select class="form-control">
                            array(1=><option>Active</option>
                           0=> <option>Not Active</option>
                            )
                          </select>
                          
     
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</div>
@stop

@extends('admin')
@section('content1')



<div class="container">
        @if (count($comments)>0)
        <h1>Comment Reply</h1>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Author</th>
            <th>Email</th>
            <th>Body</th>
        </tr>
        @foreach ($comments as $comment)


        <tr>
            <td>{{ $comment->id }}</td>
            <td>{{ $comment->author }}</td>
            <td>{{ $comment->email }}</td>
            <td>{{ $comment->body }}</td>
            <td><a href="{{ route('blog_post.home'),$comment->post->id }}">view post</td>
                <td>
                    @if ($comment->is_active==1)
                    {!! Form::open(['method'=>'PATCH','action'=>['PostCommentController@update',$comment->id]]) !!}
                    <input type="hidden" name="is_active" value="0">
                    <div class="form-group">
                        {!! Form::submit('Unapprove', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                    @else
                    {!! Form::open(['method'=>'PATCH','action'=>['PostCommentController@update',$comment->id]]) !!}
                    <input type="hidden" name="is_active" value="1">
                    <div class="form-group">
                        {!! Form::submit('Approve', ['class'=>'btn btn-info']) !!}
                    </div>
                    {!! Form::close() !!}
                    @endif
                </td>
                <td>
                        {!! Form::open(['method'=>'delete','action'=>['PostCommentController@destroy',$comment->id]]) !!}
                        <input type="hidden" name="is_active" value="1">
                        <div class="form-group">
                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                        </div>
                        {!! Form::close() !!}
                </td>
        </tr>
        @endforeach
    </table>
</thead>

@else
<h1>No comment</h1>
@endif
</div>
@stop



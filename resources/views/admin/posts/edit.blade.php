@extends('admin')

@section('content1')
<div class="container">

    <div class="row">
        <div class="col-sm-6"><h1>Edit Post</h1></div>
        <div class="col-sm-6">
                @isset($post->photos->photo)
                <li> <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image" style="height:30px;border-radius:50%;margin-top:5px;"></li>
                @endisset
                @empty($post->photos->photo)
                <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image" style="height:30px;border-radius:50%;margin-top:5px;">
                @endempty
            @php
    $photos = \App\Photo::where('photo_id',Auth::id())
                     ->first();

@endphp
            <a href="{{ url('/view_photo',$post->photos) }}"class="btn btn-primary">Edit</a>
        </div>
    </div>


{{ Form::model($post,['method'=>'patch','action'=>['AdminpostController@update',$post->id]]) }}
<div class="form-group">
        {!! Form::label('title','Title') !!}
        {!! Form::text('title',null,['class'=>'form-control'])!!}
</div>
<div class="form-group">
        {!! Form::label('category_id','Category') !!}
        {!! Form::select('category_id',array(''=>'option','0','1','2'),null,['class'=>'form-control'])!!}
</div>

<div class="form-group">
        {!! Form::label('body','Body') !!}
        {!! Form::textarea('body',null,['class'=>'form-control','rows'=>'3'])!!}
</div>
<div class="form-group">
        {!! Form::submit('Updated',['class'=>'btn btn-primary'])!!}
</div>

{!! Form::close() !!}


@stop

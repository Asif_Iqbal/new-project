@extends('admin')

@section('content1')
<div class="container">
<h1>create</h1>
{{Form::open(array('action' => 'AdminpostController@store', 'method' => 'post'))}}
<div class="form-group">
        {!! Form::label('title','Title') !!}
        {!! Form::text('title',null,['class'=>'form-control'])!!}
</div>
<div class="form-group">
        {!! Form::label('category_id','Category') !!}
        {!! Form::select('category_id',array(''=>'option','0','1','2'),null,['class'=>'form-control'])!!}
</div>

<div class="form-group">
        {!! Form::label('body','Body') !!}
        {!! Form::textarea('body',null,['class'=>'form-control','rows'=>'3'])!!}
</div>
<div class="form-group">
        {!! Form::submit('create User',['class'=>'btn btn-primary'])!!}
</div>

{!! Form::close() !!}


@stop

@extends('admin')

@section('content1')

<div class="container">

<div class="table">
<table>
<tr>
    <td>Id</td>
    <td>user</td>
    <td>categoey</td>
    <td>photo</td>
    <td>Title</td>
    <td>Body</td>
</tr>
@if($posts)
@foreach($posts as $post)
<tr>
    <th>{{ $post->id }}</th>
    @if($post->user)
     <th>{{ $post->user->name }}</th>
     @else
     <th></th>
     @endif
     @if($post)
    <th>{{ $post->category_id }}</th>
    @else
    <th></th>
    @endif
    @if($post->photos)
    <th>   <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image"></th>
    @else
    <th></th>
    @endif
    @if($post)
    <th>{{ $post->title }}</th>
    @else
    <th></th>
    @endif
    @if($post)
    <th>{{ $post->body }}</th>
    @else
    <th></th>
    @endif
    <th>
        <a href="{{ route('posts.edit',$post->id) }}" class="btn btn-primary">Edit</a>
        {{-- {{ Form::model($post,['method'=>'delete','action'=>['AdminpostController@destroy',$post->id]]) }} --}}
        <a href="{{ url('/delete',$post->id) }}" class="btn btn-danger">Delete</button>  </a>
        {{-- {!! Form:: close() !!} --}}
    </th>

</tr>

@endforeach

@endif


</table>
{{ $posts->links() }}


</div>

</div>
@stop

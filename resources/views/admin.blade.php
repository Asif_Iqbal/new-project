
<!DOCTYPE html>

<html lang="en">


<head>
@php
    $post = \App\Post::where('user_id',Auth::id())
                     ->first();

@endphp

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ url('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">

  <link rel="stylesheet" href="{{ url('css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ url('images/favicon.png') }}" />
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{ route('blog_post.home') }}"><img src="{{url ('images/logo.svg') }}" alt="logo" /></a>
        <a class="navbar-brand brand-logo-mini" href="{{ route('blog_post.home') }}"><img src="{{ url('images/logo-mini.svg') }}" alt="logo" /></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <div class="search-field d-none d-md-block">
          <form class="d-flex align-items-center h-100" action="#">
            <div class="input-group">
              <div class="input-group-prepend bg-transparent">
                <i class="input-group-text border-0 mdi mdi-magnify"></i>
              </div>
              <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">
            </div>
          </form>
        </div>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown"
              aria-expanded="false">
              <div class="nav-profile-img">

                    @isset($post->photos->photo)
                     <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image"></>
                    @endisset

                    @empty($post->photos->photo)
                    <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image" style="height:30px;border-radius:50%;margin-top:5px;">
                    @endempty

                  {{-- @foreach ($asif as $post) --}}
                {{-- @if(!is_null($post))
                <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image">
                @else
                <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image">

                @endif --}}


                <span class="availability-status online"></span>
              </div>
              <div class="nav-profile-text">
               @if (Auth::check())
               {{ Auth::user()->name }} <span class="caret"></span>
           @endif


              </div>
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                    <a href="{{ url('photo') }}" class="dropdown-item">uplod photo</a>
                <a class="dropdown-item" href="{{ route('login') }}"> <i class="mdi mdi-cached mr-2 text-success"></i>{{ __('login') }}</a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="mdi mdi-logout mr-2 text-primary"></i> {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                      @csrf
                                  </form>


              </a>




              </li>
              </a>


    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">

                    @isset($post->photos->photo)
                     <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image"
                    @endisset
                    @empty($post->photos->photo)
                    <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image" style="height:30px;border-radius:50%;margin-top:5px;">
                    @endempty

                {{-- @if(!is_null($post))
                <img src="{{ asset('images/'.$post->photos->photo) }}" alt="image">
                @else
                <img src="http://maestroselectronics.com/wp-content/uploads/bfi_thumb/blank-user-355ba8nijgtrgca9vdzuv4.jpg" alt="image">

                @endif --}}

                <span class="login-status online"></span>
                <!--change to offline or busy as needed-->
              </div>
              <div class="nav-profile-text d-flex flex-column">
                <span class="font-weight-bold mb-2"> @if (Auth::check())
                  {{ Auth::user()->name }} <span class="caret"></span>
              @endif

              </div>
              <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
          </>

          <li class="nav-item">
            <a class="nav-link" href="{{ url('admin/users') }}">
              <span class="menu-title">Users</span>
              <i class="mdi mdi-home menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <span class="menu-title">category</span>
              <i class="menu-arrow"></i>
              <i class="mdi mdi-crosshairs-gps menu-icon"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="{{url ('admin/users/create') }}">create</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ url('admin/posts') }}">Admin post</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ url('admin/posts/create') }}">Admin post create</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ url('admin/posts/edit') }}">Admin post edit</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="{{ url('comment') }}">
              <span class="menu-title">Comment</span>
              <i class="mdi mdi-contacts menu-icon"></i>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a class="nav-link" href="">
              <span class="menu-title">Forms</span>
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">
              <span class="menu-title">Charts</span>
              <i class="mdi mdi-chart-bar menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">
              <span class="menu-title">Tables</span>
              <i class="mdi mdi-table-large menu-icon"></i>
            </a>
          </li> --}}
        </ul>

      </nav>


      <!-- plugins:js -->
      <script src="{{ url('vendors/js/vendor.bundle.base.js') }}"></script>
      <script src="{{url ('vendors/js/vendor.bundle.addons.js') }}"></script>
      <script src="{{ url('js/off-canvas.js') }}"></script>
      <script src="{{url ('js/misc.js') }}"></script>
      <!-- endinject -->
      <!-- Custom js for this page-->
      <script src="{{url ('js/dashboard.js') }}"></script>
      @yield('content1')
      <!-- End custom js for this page-->
</body>

</html>

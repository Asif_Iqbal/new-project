@extends('admin')

@section('content1')

    <div class="container">

        <h1>Update Photo</h1>
        {{-- {{ Form::model(['method'=>'post','action'=>['PhotoController@update_photo']]) }}
   {{csrf_field()}}
    <div class="form-group">
        {!! Form::label('photo',$photos->photo) !!}
        {!! Form::file('photo',null,['class'=>'form-control'])!!}
    </div>
    <div class="form-group">
    {{ Form::submit('Edit',['class'=>'btn btn-danger']) }}
    </div>

        {{ Form::close() }} --}}

        <form action="{{ route('update.photo',$photos->id) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}

                 <input type="file"name="photo" accept="image/*">
                <input type="submit" value="submit">
        </form>

</div>
@stop

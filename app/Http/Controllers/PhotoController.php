<?php

namespace App\Http\Controllers;

use Auth;
use App\Photo;



use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PhotoController extends Controller
{
    public function show()
    {
        return view('user_photo');
    }
    public function view(Request $request)
    {

        if ($request->hasFile('photo')) {

            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('/images/' . $filename);
            Image::make($image)->resize(1000, 800)->save($location);
        }


        $photos = new Photo;
        $photos->photo_id = Auth::id();
        $photos->photo = $filename;
        $photos->save();
    }

    public function photo($id)
    {
        $photos = Photo::findOrFail($id);
        return view('photo_show', compact('photos'));
    }
    public function update_photo(Request $request, $id)
    {


        // $photos = Photo::find($id);
        // $photos->photo_id = Auth::id();

        // if ($request->hasFile('photo')) {
        //     $file = $request->file('photo');
        //     $extension = $file->getClientOriginalExtension();
        //     $filename = time() . '.' . $extension;
        //     $file = move('/images', $filename);
        //     $photos->photo = $filename;
        // }
        // $photos->save();

        // $photos = Photo::find($id);
        // $this->validate($request, array(
        //     'photo' => 'image'
        // ));

        $photos = Photo::find($id);

        if ($request->hasFile('photo')) {
            $image = $request->file('photo');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(1000, 800)->save($location);
            $photos->photo = $filename;
            $photos->save();
        }
        // $image = $request->file('photo');
        // $filename = time() . '.' . $image->getClientOriginalExtension();


        // $photos->photo_id = Auth::id();


        // if (isset($request->tags)) {
        //     $photos->tags->sync($request->tags);
        // } else {
        //     $photos->tags->sync(array());
        // }
    }

    // public function view_image()
    // {
    //     $photo = Photo::all();
    //     return view('', compact('photo'));
    // }
}

<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;

use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    public function blog_post()
    {
        $posts = Post::orderBy('id', 'DESC')->paginate(1);
        return view('blog_post', compact('posts'));
    }
}

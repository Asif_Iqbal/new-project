<?php

use App\Post;
use App\Photo;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/photo', 'PhotoController@show');
Route::post('/view_photo', 'PhotoController@view')->name('view_photo.show');




Route::group(['middleware' => 'payel'], function () { 


});

Route::resource('admin/users', 'AdminuserController');


Route::get('/show', function () {
    $views = Post::all();
    return View('admin', compact('views'));
});

Route::resource('admin/posts', 'AdminpostController');


Route::get('/delete/{id}', function ($id) {
    $post = Post::findOrFail($id);
    $post->delete();
    return redirect('admin/posts');
});

Route::get('/view_photo/{id}', 'PhotoController@photo')->name('view.photo');

Route::post('/view/{id}', 'PhotoController@update_photo')->name('update.photo');

Route::get('/edit', function ($id) {
    $photos = Photo::findOrFail($id);
    return view('photo_show', compact('photos'));
});
Route::resource('/comment', 'PostCommentController');
Route::resource('/comment_reply', 'ReplyCommentController');

route::get('/blog_post', 'BlogPostController@blog_post')->name('blog_post.home');
route::get('/user_comment', 'PostCommentController@view_comment');
